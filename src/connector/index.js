module.exports = {
    /**
     * Connector
     *
     */
    MongoDB: require('./MongoDB'),
    Redis: require('./Redis'),
}
